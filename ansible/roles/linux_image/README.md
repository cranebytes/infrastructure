# linux_image

This is a **minimal** role.

## Description
Install kernel from backports. This role will reboot the machine if a new
kernel is installed.