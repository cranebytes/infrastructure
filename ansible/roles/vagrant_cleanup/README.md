# vagrant_cleanup

This is a **minimal** role.

## Description
Cleanup a packer vagrant build before exporting it to reduce image size.